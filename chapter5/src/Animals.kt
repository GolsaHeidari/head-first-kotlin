open class Animal{
    open val image = ""
    open val food = ""
    open val habitat = ""
    val hunger = 10

    open fun makeNoise(){
        println(" The animal is making noise")
    }
    open fun eat(){
        println(" The animal is eating")
    }
    open fun roam(){
        println(" The animal is roaming")
    }
    fun sleep(){
        println(" The animal is sleeping")
    }

}

class Hippo(): Animal() {
    override val image = "hippo.jpg"
    override val food = "grass"
    override val habitat = "water"

    override fun makeNoise() {
        println("Grunt! Grunt!")
    }

    override fun eat() {
        println("Hippo is eating $food")
    }
}

open class Canine(): Animal(){
    override fun roam() {
        println(" The canine is roaming")
    }
}

class Wolf(): Canine(){
    override val image = "wolf.jpg"
    override val food = "meat"
    override val habitat = "forests"

    override fun makeNoise() {
        println("Hooooow!")
    }
    override fun eat() {
        println("Wolf is eating $food")
    }
     }

class Vet() {
    fun giveShot(animal: Animal){
        animal.makeNoise()
    }
}


fun main(args:Array<String>) {
    val animals = arrayOf(Hippo(), Wolf())
    for (item in animals) {
        item.roam()
        item.eat()
    }

    val vet = Vet()
    val wolf = Wolf()  // Wolf is of class canine and is a subclass of animal
    val hippo = Hippo()
    vet.giveShot(wolf)
    vet.giveShot(hippo)
}
