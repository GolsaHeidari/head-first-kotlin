// A3, B3
open class Monster {
    open fun frighten() : Boolean {
        println("Aargh!")
        return true
    }
}

class Vampyre : Monster() {
    fun beScary() : Boolean {// As Vampyre does not have the function "frighten", compiler moves to the supercless of it, monster, and "Aargh" would be printed
        println("Fancy a bite?")
        return true
    }
}

class Dragon : Monster() {
    override fun frighten(): Boolean {
        println("Fire!")
        return true
    }
}

fun main(){
    val m = arrayOf(Vampyre(),
        Dragon(),
        Monster())

    for (item in m) {
        item.frighten()
    }
}