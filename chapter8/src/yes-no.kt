import kotlin.Exception

class BadException2:Exception()

fun myFunction2(test : String){
    print("th")
    try {
        riskyCode2(test)
        print("o")
    }
    catch (e:BadException2){
        print("a")
    }
    finally {
        print("ws-")
    }
}

fun riskyCode2(test: String){
    if (test == "yes"){
        throw BadException2()
    }
}
fun main(){
    var test1 = "yes"
    myFunction(test1)
    var test2 = "no"
    myFunction(test2)
}

