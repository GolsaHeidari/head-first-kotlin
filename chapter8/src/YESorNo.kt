import kotlin.Exception

class BadException : Exception()

fun myFunction(test:String){
    try {
        print("t")
        riskyCode(test)
        print("o")
        }
    catch (e:BadException){
        print("a")
    }
    finally {
        print("w")
    }
    print("s-")
}
fun riskyCode(test: String){
    print("h")
    if (test == "yes"){
        throw BadException()
    }
    print("r")
}

fun main(){
    var test1 = "yes"
    myFunction(test1)
    var test2 = "no"
    myFunction(test2)
}