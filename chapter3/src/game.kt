fun main (args: Array<String>){
    val a1 = arrayOf("rock","paper","scissors")
//    for ((index,item) in a1.withIndex())
//        println("$item is in the $index of the list")
    val o1 = getGameChoice(a1)

    val o2 = getUserChoice(a1)

    showResult(o2,o1)

}

fun getGameChoice(option : Array<String>) = option[(Math.random() * option.size).toInt()]

fun getUserChoice(option : Array<String>):String{
    var valid=false
    var userchoice=""
    while (!valid) {
        print("Please enter one of the items: ")
        for (item in option)
            print("$item ")
        var x = readLine()
        x = x?.toLowerCase()
        if ( x != null &&x in option)
        {   valid = true
            userchoice = x}
        else
            println("it is not valid")
    }

    return userchoice
}

fun showResult(a: String, b :String){
    val result:String
    if (a==b) result="tie!"
    else {
        if ((a == "rock" && b == "scissors") || (a == "paper" && b == "rock") || (a == "scissors" && b == "paper"))
            result = "You win"
        else result = "You lost"
    }

    println("You chose $a and I chose $b. So $result")
}