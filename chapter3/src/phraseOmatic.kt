fun main(args: Array<String>){
    val a1 = arrayOf("24.7","multi","d","e","f","g")
    val a2 = arrayOf("84,6","man","k","l","n")
    val a3 = arrayOf("16.3","woman","s","t")

    val s1 = a1.size
    val s2 = a2.size
    val s3 = a3.size

    val rand1 = (Math.random() * s1).toInt()
    val rand2 = (Math.random() * s2).toInt()
    val rand3 = (Math.random() * s3).toInt()

    val phrase = "${a1[rand1]} ${a2[rand2]} ${a3[rand3]}"

    println(phrase)
}