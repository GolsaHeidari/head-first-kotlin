class Songs (val title: String, val artist: String) {
    fun  play(){
        println("Playing the song $title by $artist")
    }
    fun stop(){
        println("Stopped the song $title by $artist")
    }
}

fun main(args: Array<String>){
    val song1 = Songs("The Mesopotamians", "Thay might be giants")
    val song2 = Songs("Going underground", "The Jam")
    val song3 = Songs("Make me smile", "Steve Harley")

    song1.play()
    song2.stop()
    song3.play()

    println(song1.title)
    println(song2.title)
    println(song3.title)

}