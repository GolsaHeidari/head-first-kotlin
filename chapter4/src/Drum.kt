class DrumKit(var hasTopHAt: Boolean, var hasSnar: Boolean){
    fun playTopHat() {
        if (hasTopHAt) println("ding ding ba-da-bing!")
    }
    fun playSnar(){
        if (hasSnar) println("bang bang bang!")
    }
}
fun main(args: Array<String>){
    val d=DrumKit(true, true)
    d.playTopHat()
    d.playSnar()
    d.hasSnar = false
    d.playTopHat()
    d.playSnar()

}