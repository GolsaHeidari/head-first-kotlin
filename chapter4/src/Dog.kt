fun main(args: Array<String>){
    val  mydog = Dog ("Skai", 10, "Mixed")
    // println("My dog is ${mydog.name}")
    mydog.bark()
    mydog.weight = 75
    println("Weight in Kiloograms is ${mydog.weightToKgr}")
    mydog.weight = -2
    println("Weight is ${mydog.weight}")

    mydog.activities = arrayOf("stands", "eats", "sleeps")
    for (item in mydog.activities)
        println("${mydog.name} enjoys $item")

    val dogs = arrayOf(Dog("Fido", 20, "Mixed"), Dog("Gipsy", 50, "Poolde"))
    dogs[1].bark()
    dogs[1].weight = 15
    println("Weight for ${dogs[1].name} is ${dogs[1].weight}")

}

class Dog (val name: String, weight_param: Int, breed_param: String){
    init {
        println("Dog $name has been created")
    }

    var activities= arrayOf("walks")

    val breed = breed_param.toUpperCase()

    init {
        println("The breed is :$breed")
    }

    var weight = weight_param
        set(value) {
            if (value > 0) field = value
        }
    val weightToKgr : Double
        get() = weight / 2.2

    fun bark() {
        println(if (weight < 20) "Yip!" else "Woof")
    }
}
