package chapter09
fun main() {
    val petsTypeLiam = listOf("Cat", "Dog", "Fish", "Fish")
    val petsTypeSofia = listOf("Cat", "Owl")
    val petsTypeNoah = listOf("Dog", "Dove", "Dog", "Dove")
    val petsTypeEmily = listOf("Hedgehog")

    // 1
    var petsType = petsTypeLiam.toMutableSet()
    petsType.addAll(petsTypeSofia)
    petsType.addAll(petsTypeNoah)
    petsType.addAll(petsTypeEmily)

//    var petsType = mutableSetOf<String>()
//    val listPet = listOf(petsTypeLiam, petsTypeSofia, petsTypeNoah, petsTypeEmily)
//
//    listPet.forEach { it -> petsType.addAll(it) }

    println(petsType)
    println(petsType.size)

    // 2
    var pets = mutableListOf<String>()
    val listPet = listOf(petsTypeLiam, petsTypeSofia, petsTypeNoah, petsTypeEmily)
    listPet.forEach { it -> pets.addAll(it) }

    // 3
    println(pets)
    println(pets.size)

    // 4
    var petsTypeordered = petsType.toMutableList().sorted()
    println(petsTypeordered)

}