package chapter09
fun main(args: Array<String>) {
    val set = setOf(Duck(), Duck(17))
    println(set)
}

 //1 ( follows the hasCode and equals rules)
class Duck(val size: Int = 17) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        if (other is Duck && size == other.size) return true
        return false
    }

    override fun hashCode(): Int {
        return size
    }
}

//// 2 (This produces a Set with two items. The class beaks the hashCode and equals rules)
//class Duck(val size: Int = 17) {
// override fun equals(other: Any?): Boolean {
// return false
// }
//
// override fun hashCode(): Int {
// return 7
// }
//}

//// 3 (This follows the rules, but produces a set with two items.)
//data class Duck(val size: Int = 18)

// 4 (Break the rules of hashCode)
//class Duck(val size: Int = 17) {
// override fun equals(other: Any?): Boolean {
// if (this === other) return true
//
// if (other is Duck && size == other.size) return true
// return false
// }
//
// override fun hashCode(): Int {
// return (Math.random() * 100).toInt()
// }
