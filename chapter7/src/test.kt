class test {
    class classNomal (val name : String) {
        init {
            print("My name is  $name \n")
        }

        override fun equals(other: Any?): Boolean {
            if (other == null) return false
            return if (other is classNomal) other.name == this.name
            else false
//        return super.equals(other)
        }
    }
    data class dataClass (val name : String) {
        init {
            print("hey I'm a data class and my name is  $name \n")
        }
    }


    fun main(){
        var Salomon = classNomal(name = "Salomon")
        var SalomonCopy = dataClass("Salomon")
        print(message = false)
    }



}