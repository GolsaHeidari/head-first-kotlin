data class movie(val title :String, val year: String)

class song(val title: String, val artist:String)

fun main(args: Array<String>){
    var m1 = movie("Black panther", "2018")
    var m2 = movie("Jurassic Park", "2015")
    var m3 = movie("Jurassic Park", "2015")
    var s1 = song("Love Cats", "The Cure")
    var s2 = song("Wild horses", "The rolling stone")
    var s3 = song("Love Cats", "The Cure")

    println(m2==m3)
    println(s1==s3)

    var m4 = m1.copy()
    println(m1==m4)

    var m5 = m1.copy()
    println(m1===m5)
    var m6 = m2
    m2 = m3
    println(m3==m6)

}