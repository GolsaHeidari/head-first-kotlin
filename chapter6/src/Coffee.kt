abstract class Applience(){
    var pluggedIn = true
    abstract val color : String
    abstract fun consumPower()
}

class CoffeeMaker : Applience(){
    override val color=""
    var coffeeLeft = false
    override fun consumPower() {
        println("Consuming power")
    }
    fun fillWithWater(){
        println("Fill with water.")
    }
    fun makeCoffee(){
        println("Make the coffee")
    }
}

fun main() {
   val coffeeMaker: CoffeeMaker = CoffeeMaker()
    coffeeMaker.fillWithWater()
    coffeeMaker.makeCoffee()

}