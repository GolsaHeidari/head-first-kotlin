import Version1.Cat
import Version1.Fish
import Version1.Pet
import Version1.Vet


class Contest<T: Pet>(var vet: Vet<in T>) {
    val scores: MutableMap<T, Int> = mutableMapOf()

    fun addScore(t: T, score: Int = 0) {
        if (score >= 0) scores.put(t, score)
    }

    fun getWinners(): MutableSet<T> {
        val winners: MutableSet<T> = mutableSetOf()
        val highScore = scores.values.max()
        for ((t, score) in scores) {
            if (score == highScore) winners.add(t)
        }
        return winners
    }
}

interface Retailer<out T>{
        fun sell():T
    }

    class CatRetailer : Retailer<Version1.Cat>{
        override fun sell(): Version1.Cat {
            println("Sell cat.")
            return Version1.Cat("")
        }
    }

    class DogRetailer : Retailer<Version1.Dog>{
        override fun sell(): Version1.Dog {
            println("Sell dog.")
            return Version1.Dog("")
        }
    }

    class FishRetailer : Retailer<Version1.Fish>{
        override fun sell(): Version1.Fish {
            println("Sell fish.")
            return Version1.Fish("")
        }
    }

fun main(args: Array<String>){
    val catFuzz = Version1.Cat("Fuzz Lightyear")
    val catKatsu = Version1.Cat("Katsu")
    val fishFinny = Version1.Fish("Finny McGrow")

    val getCatVet = Vet<Version1.Cat>()
    val getFishVet = Vet<Version1.Fish>()
    val getPetVet = Vet<Version1.Pet>()

    val catRetailer1 = CatRetailer()
    val catRetailer2: CatRetailer = CatRetailer()

    getCatVet.treat(catFuzz)
    getPetVet.treat(catKatsu)
    getPetVet.treat(fishFinny)

    val catContest = Contest<Cat>(getCatVet)
    catContest.addScore(catFuzz, 50)
    catContest.addScore(catKatsu, 45)
    val topCat = catContest.getWinners().first()
    println("Version1.Cat contest winner is ${topCat.name}")

    val petContest = Contest<Pet>(getPetVet)
    petContest.addScore(catFuzz, 50)
    petContest.addScore(fishFinny, 56)
    val topPet = petContest.getWinners().first()
    println("Version1.Pet contest winner is ${topPet.name}")

    val fishContest = Contest<Fish>(getPetVet)
    val dogRetailer: Retailer<Version1.Dog> = DogRetailer()
    val catRetailer: Retailer<Version1.Cat> = CatRetailer()
    val petRetailer: Retailer<Version1.Pet> = CatRetailer()
    petRetailer.sell()
}
