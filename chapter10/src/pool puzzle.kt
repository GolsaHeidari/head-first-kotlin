import Version1.Cat
import Version1.Fish
import Version1.Pet

class PetOwner<T: Pet>(t:T){
    val pets = mutableListOf(t)

    fun add(t:T){
        pets.add(t)
    }

    fun remove(t:T){
        pets.remove(t)
    }
}

fun main(args: Array<String>){
    val catFuzz = Cat("Fuzz Lightyear")
    val catKatsu = Cat("Katsu")
    val fishFinny = Fish("Finny McGrow")
    val catOwner = PetOwner(catFuzz)
    catOwner.add(catKatsu)
    for (item in catOwner.pets)
    println("He is the owner of ${item.name}")
}