abstract class Pet(var name: String)
class Cat(name: String) : Pet(name)
class Dog(name: String) : Pet(name)
class Fish(name: String) : Pet(name)
class Contest<T: Pet>(){
    var scores: MutableMap<T, Int> = mutableMapOf()
    fun addscore(t: T, score: Int =0){
        if (score >= 0) scores.put(t, score)
    }

    fun getWinners(): Set<T> {
        val highScore = scores.values.max()
        val winners = scores.filter { it.value == highScore }.keys
        winners.forEach { println("Winner : ${it.name}") }
        return winners
    }
}

fun main(args: Array<String>){

    val myCat = arrayOf(Cat("c1"), Cat("c2"), Cat( "c3"))
    val myDog = arrayOf(Dog("d1"), Dog("d2"))
    val myFish = arrayOf(Fish("f1"))


}
