package chapter12

fun main(){
    val myMap = mapOf("A" to 4, "B" to 3, "C" to 2, "D" to 1, "E" to 2)

    var x1 = ""
    var x2 = 0

    // candidate 1 // ABCDE0
//    x1 = myMap.keys.fold("") { x, y -> x + y }
//    x2 = myMap.entries.fold(0) { x, y -> x * y.value }

    // candidate 2, compute the sum of the keys [1, 2, 3, 4]
//    x2 = myMap.values.groupBy { it }.keys.sumBy { it }

    // candidate 3
//    x1 = "ABCDE"
//    x2 = myMap.values.fold(12) { x, y -> x - y }

    // candidate 4
//    x2 = myMap.entries.fold(1) {x, y -> x * y.value}

//    // candidate 5
//    x1 = myMap.values.fold("") { x, y -> x + y }
//
//    // condidate 6
    x1 = myMap.values.fold(0) { x, y -> x + y }.toString()
    x2 = myMap.keys.groupBy { it }.size

    println("$x1$x2")
}