data class Grocery( val name: String, val category: String, val unit: String, val unitPrice: Double, val quantity: Int)

fun main(args: Array<String>) {
    val groceries = listOf(
        Grocery("Tomatoes", "Vegetable", "lb", 3.0, 3),
        Grocery("Mushrooms", "Vegetable", "lb", 4.0, 1),
        Grocery("Bagels", "Bakery", "Pack", 1.5, 2),
        Grocery("Olive Oil", "Pantry", "Bottle", 6.0, 1),
        Grocery("Ice cream", "Frozen", "Pack", 3.0, 2)
    )
    val highestUnitPrice = groceries.maxBy { it.unitPrice }
    println("highestUnitPrice: $highestUnitPrice")

    val lowestQuantity = groceries.minBy { it.quantity }
    println("lowestQuantity: $lowestQuantity")

    val sumQuantity = groceries.sumBy { it.quantity }
    println("sumQuantity: $sumQuantity")

    val totalPrice = groceries.sumByDouble { it.unitPrice * it. quantity }
    println("totalPrice: $totalPrice")

    val vegetables = groceries.filter { it.category == "Vegetable" }
    println("vegetables: $vegetables")

    val unitPriceOver3 = groceries.filter { it.unitPrice > 3 }
    val notFrozen = groceries.filterNot { it.category == "Frozen" }
    println("notFrozen: $notFrozen")

    val groceryName = groceries.map { it.name }
    println("groceryName: $groceryName")

    val halfPrice = groceries.map { it.unitPrice * 0.5 }
    println("halfPrice: $halfPrice")

    val newPrice = groceries.filter { it.unitPrice > 3 }.map { it.unitPrice * 2 }
    println("newPrice: $newPrice")


    println("Grocery name:")
    groceries.forEach { println(it.name) }

    println("Groceris with unitPPrice > 3:")
    groceries.filter { it.unitPrice > 3 }.forEach {println(it.name)}

    var itemNames = ""
    groceries.forEach ( {itemNames+= "${it.name}"})
    print("itemNames: $itemNames")

    println("--------------------")

    for (item in groceries){
        if (item.unitPrice > 3) print(item.name)
    }

    var itemName = ""
    for (item in groceries){
        itemName += "${item.name}"
    }
    print("itemName: $itemName")

    println("-------------------")

    groceries.groupBy { it. category }.forEach{
        println("${it.key}:")
        it.value.forEach { println("       - ${it.name}") }
    }

    println("-------------------")

    val ints = listOf(1, 2, 3, 4)
    val sum = ints.fold(0){ runningSum, item -> runningSum + item}
    println("runningSum: $sum")
    val Product = ints.fold(1){ runningProduct, item -> runningProduct * item}
    println("runningProduct: $Product")
    val x=ints.sum()
    println(x)


    val names = groceries.fold(""){string, item -> string + "${item.name}"}
    println("names: $names")

    val chanhe50 = groceries.fold(50.0){change, item -> change - item.unitPrice * item.quantity}
    println("chanhe50: $chanhe50")

    println("-------------------")

    groceries.filter { it.category == "Vegetable" }.sumByDouble { it.unitPrice * it.quantity }

    groceries.filter{ it.unitPrice * it.quantity > 5.0 }.map { it.name }

    groceries.groupBy { it.category }.forEach{println("${it.key} ${it.value.sumByDouble { it.unitPrice * it.quantity }}")}

    groceries.filterNot { it.unit == "Bottle" }.groupBy { it.unit }.forEach { println(it.key)
        it.value.forEach { "${it.name}"} }


    // Write the code below to find out how much will be spent on vegetables.
    val spentOnVegetable = groceries.filter { it.category == "Vegetable" }
        .sumByDouble { it.quantity * it.unitPrice }
    println("You spend a total of ${spentOnVegetable} on Vegetables")

    // Create a List containing the name of each item whose total price is less than 5.0
    val listNameLess5 = groceries.filter {
        it.unitPrice * it.quantity  < 5.0
    }.map { it.name }
    println("Here is a list containing the name of each item whose total price is less than 5.0: ")
    println(listNameLess5)

    // Print the total cost of each category
    groceries.groupBy { it.category }.forEach{
        println(it.key)
        println(it.value.sumByDouble { it.unitPrice * it.quantity })
    }

    // Print the name of each item that doesn't come in a bottle, grouped by unit.
    groceries.filterNot { it.unit == "Bottle" }
        .groupBy { it.unit }.forEach {
            println(it.key)
            it.value.forEach { println(" ${it.name}") }
        }




}
