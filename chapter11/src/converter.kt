typealias DoubleConversion = (Double) -> Double

fun convert(x: Double,
            converter: DoubleConversion) : Double{
    val result = converter(x)
    println("$x is converted to $result")
    return result
}

//fun convertFive(converter: (Int) -> Double) : Double {
//    val result = converter(5)
//    println(" 5 is converted to $result")
//    return result
//}

fun getConversionLambda(str: String): DoubleConversion {
    if (str == "CentigradeToFahrenheit"){
        return{ it * 1.8 + 32}
    } else if (str == "KgsToPounds"){
        return{ it * 2.204623}
    } else if (str == "PoundsToUSTones"){
        return{ it / 2000}
    } else {
        return {it}
    }
}

fun combine( lambda1: DoubleConversion,
             lambda2: DoubleConversion ): DoubleConversion {
    return { x: Double -> lambda2(lambda1(x)) }
}

fun main(args: Array<String>){

//convert( 20.0 ) { it * 1.8 + 32}
//convertFive { it * 1.8 + 32}
    println("Convert 2.5Kg to Pounds: ${getConversionLambda("KgsToPounds")(2.5)}")

    //Define two conversion lambdas
    val KgsToPoundsLambda = getConversionLambda("KgsToPounds")
    val KgsToUSPoundsLambda = getConversionLambda("PoundsToUSTones")

    //Combine the two lambdas to create a new one
    val KgsToUSTonsLambda = combine(KgsToPoundsLambda, KgsToUSPoundsLambda)

    val value = 17.4
    println("$value kgs is ${convert(value, KgsToUSTonsLambda)} US Tones")

//    convert(20.0, getConversionLambda("KgsToPounds"))
}