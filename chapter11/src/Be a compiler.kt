fun myFun3(x: Int = 6, y:(Int) -> Int = {x: Int -> x + 6}): Int {
    return y(x)
}

fun myFun4(x: Int, y:Int, z:(Int, Int) -> Int = {x: Int, y:Int -> x + y}){
    z(x, y)
}

fun myFun5(x: (Int) -> Int = {println(it)
    it + 7}){
    x(8)
}

fun main(arg: Array<String>){
    val mf3 = myFun3()
    println("$mf3")

    val mf4 = myFun4(7,3)
    println("$mf4")

    val mf5 = myFun5()
    println("$mf5")
}